package Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.afzal.icare.R;

import java.util.List;

import Database.DoctorProfileModel;
import Database.UserProfileModel;

public class ProfileAdapter extends ArrayAdapter<UserProfileModel> {

    public ProfileAdapter(Context context, List<UserProfileModel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserProfileModel model= getItem(position);
        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.listview_item_layout,parent,false);
        }

        TextView nameTV = (TextView) convertView.findViewById(R.id.textView);



        nameTV.setText(model.getName());


        return  convertView;
    }

}
