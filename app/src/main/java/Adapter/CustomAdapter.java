package Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.afzal.icare.R;

import java.util.List;

import Database.DoctorProfileModel;

public class CustomAdapter extends ArrayAdapter<DoctorProfileModel> {

    public CustomAdapter(Context context, List<DoctorProfileModel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DoctorProfileModel model = getItem(position);
        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.doctor_list,parent,false);
        }

        TextView nameTV = (TextView) convertView.findViewById(R.id.docNameTV);
        TextView detailTV = (TextView) convertView.findViewById(R.id.docDetailTV);


        nameTV.setText(model.getDoctorName());
        detailTV.setText(model.getDoctorDetail());

        return  convertView;
    }

}
