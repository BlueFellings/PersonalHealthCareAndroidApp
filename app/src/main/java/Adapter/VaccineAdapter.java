package Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.afzal.icare.R;
import java.util.List;
import Database.VaccineModel;

public class VaccineAdapter extends ArrayAdapter<VaccineModel> {

    public VaccineAdapter(Context context, List<VaccineModel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VaccineModel model = getItem(position);
        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.vaccination_chart,parent,false);
        }
        TextView vaccTypeTV = (TextView) convertView.findViewById(R.id.vaccTypeTV);
        TextView vaccDetailTV = (TextView) convertView.findViewById(R.id.vaccDetailsTV);
        TextView vaccTimeTV = (TextView) convertView.findViewById(R.id.vaccTimeTV);
        TextView vaccDateTV = (TextView) convertView.findViewById(R.id.vaccDateTV);

        vaccTypeTV.setText(model.getVaccineName());
        vaccDetailTV.setText(model.getVaccineDetails());
        vaccTimeTV.setText(model.getVaccineTime());
        vaccDateTV.setText(model.getVaccineDate());

        return convertView;
    }
}
