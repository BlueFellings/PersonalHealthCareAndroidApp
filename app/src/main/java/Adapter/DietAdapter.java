package Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.afzal.icare.R;
import java.util.List;
import Database.DietModel;

public class DietAdapter extends ArrayAdapter<DietModel> {

    public DietAdapter(Context context, List<DietModel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DietModel model = getItem(position);
        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.diet_chart,parent,false);
        }
        TextView dietTypeTV = (TextView) convertView.findViewById(R.id.viewDietTypeTV);
        TextView dietMenuTV = (TextView) convertView.findViewById(R.id.viewDietMenuTV);
        TextView dietTimeTV = (TextView) convertView.findViewById(R.id.viewDietTimeTV);
        TextView dietDateTV = (TextView) convertView.findViewById(R.id.viewDietDateTV);

        dietTypeTV.setText(model.getType());
        dietMenuTV.setText(model.getMenu());
        dietTimeTV.setText(model.getTime());
        dietDateTV.setText(model.getDate());

        return convertView;
    }
}
