package Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.afzal.icare.R;

import java.io.ByteArrayInputStream;
import java.util.List;

import Database.MedicalHistoryModel;

public class MedicalAdapter extends ArrayAdapter<MedicalHistoryModel> {


    public MedicalAdapter(Context context,  List<MedicalHistoryModel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MedicalHistoryModel historyModel = getItem(position);


        if (convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.medical_history,parent,false);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.medicalViewImage);
        TextView doctorName = (TextView) convertView.findViewById(R.id.doctorName);
        TextView details = (TextView) convertView.findViewById(R.id.medicalDetails);
        TextView date = (TextView) convertView.findViewById(R.id.medicalDate);

        byte[] outImage=historyModel.getImage();
        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);

        image.setImageBitmap(theImage);
        doctorName.setText(historyModel.getMedicalDoctorName());
        details.setText(historyModel.getMedicalDetail());
        date.setText(historyModel.getMedicalDate());



        return convertView;
    }
}
