package com.example.afzal.icare;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import Adapter.ProfileAdapter;
import Database.UserDataResource;
import Database.UserProfileModel;


public class UserProfileList extends ActionBarActivity {

    ListView listView;
    UserDataResource userDataResource;
    ArrayList<UserProfileModel>allProfileList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_list);
        listView = (ListView) findViewById(R.id.userProfileList);
        userDataResource = new UserDataResource(this);
        allProfileList = userDataResource.getAllContact();


        ProfileAdapter adapter = new ProfileAdapter(getApplicationContext(),allProfileList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserProfileModel userProfileModel = allProfileList.get(position);
                Intent intent = new Intent(UserProfileList.this, ProfileDetailsActivity.class);
                intent.putExtra("id1", userProfileModel.getId());
                intent.putExtra("name", userProfileModel.getName());
                intent.putExtra("age", userProfileModel.getAge());
                intent.putExtra("height", userProfileModel.getHeight());
                intent.putExtra("weight", userProfileModel.getWeight());
                intent.putExtra("relation", userProfileModel.getRelation());
                intent.putExtra("bloodGroup", userProfileModel.getBloodGroup());
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#70a7da")));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.diet_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_diet) {
            Intent intent = new Intent(this,AddProfileActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }


        return super.onOptionsItemSelected(item);
    }
}
