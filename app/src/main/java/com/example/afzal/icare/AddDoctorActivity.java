package com.example.afzal.icare;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import Database.DoctorDataResource;
import Database.DoctorProfileModel;

public class AddDoctorActivity extends Activity{

    EditText docNameET,docDetailsET,docPhoneET,docEmailET;
    String name,details,appoinment,phone,email;
    DoctorDataResource doctorDataResource;

    Calendar calendar1;
    EditText pickDate;
    int day, month, year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_doctor_profile);
        docNameET = (EditText) findViewById(R.id.DnameET);
        docDetailsET = (EditText) findViewById(R.id.DoctorDetailsET);
        pickDate = (EditText) findViewById(R.id.appMenttET);
        docPhoneET = (EditText) findViewById(R.id.DphoneET);
        docEmailET = (EditText) findViewById(R.id.DemailET);

        calendar1 = Calendar.getInstance();
        year = calendar1.get(Calendar.YEAR);
        month = calendar1.get(Calendar.MONTH);
        day = calendar1.get(Calendar.DAY_OF_MONTH);
        doctorDataResource = new DoctorDataResource(this);
    }
    public void setdDate(View view) {
        showDialog(999);

    }


    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }

        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {

        pickDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    public void addDoctor(View v){
        name = docNameET.getText().toString();
        details = docDetailsET.getText().toString();
        appoinment = pickDate.getText().toString();
        phone = docPhoneET.getText().toString();
        email = docEmailET.getText().toString();

        DoctorProfileModel doctorProfileModel = new DoctorProfileModel(name,details,appoinment,phone,email);
        boolean inserted = doctorDataResource.insert(doctorProfileModel);
        if (inserted){
            Toast.makeText(getApplicationContext(),"inserted",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(),NextPartActivity.class);
            startActivity(intent);
        }else
        {
            Toast.makeText(getApplicationContext(),"inserted Failed",Toast.LENGTH_LONG).show();
        }

    }

}
