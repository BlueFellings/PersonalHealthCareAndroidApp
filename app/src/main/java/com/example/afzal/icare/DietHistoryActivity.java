package com.example.afzal.icare;



import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import Adapter.DietAdapter;
import Database.DietDataResource;
import Database.DietModel;

public class DietHistoryActivity extends ActionBarActivity {

    ListView dietList;
    ArrayList<DietModel>allDiet = new ArrayList<>();
    DietDataResource dietDataResource;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_diet_chart);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF7BB159")));

        dietList = (ListView) findViewById(R.id.dietList);
        dietDataResource = new DietDataResource(this);

        allDiet = dietDataResource.getAllContact();

        DietAdapter adapter = new DietAdapter(getApplicationContext(),allDiet);
        dietList.setAdapter(adapter);

        dietList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 final DietModel dietModel = allDiet.get(position);

                final CharSequence[] items = { "Delete", "Edit", "Cancel" };
                AlertDialog.Builder builder = new AlertDialog.Builder(DietHistoryActivity.this);
                builder.setTitle("Delete");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Delete")) {

                            dietDataResource.deleteContact(dietModel.getId());
                            Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(DietHistoryActivity.this,NextPartActivity.class);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                        } else if (items[item].equals("Edit")) {

                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }

        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.diet_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_diet) {
            Intent intent = new Intent(this,AddDietActivity.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }
}
