package com.example.afzal.icare;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import Adapter.CustomAdapter;
import Database.DoctorDataResource;
import Database.DoctorProfileModel;
import Database.UserDataResource;


public class DoctorHistoryActivity extends ActionBarActivity {
    ListView doctorList;
    ArrayList<DoctorProfileModel> allDetails = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_doctor_list);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#D0F5573C")));

        doctorList = (ListView) findViewById(R.id.doctorList);
        DoctorDataResource doctorDataResource = new DoctorDataResource(this);

        allDetails = doctorDataResource.getAllContact();

        CustomAdapter adapter = new CustomAdapter(getApplicationContext(),allDetails);
        doctorList.setAdapter(adapter);

        doctorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DoctorProfileModel doctorProfileModel = allDetails.get(position);

                Intent intent = new Intent(DoctorHistoryActivity.this,DoctorProfileDetailsActivity.class);
                intent.putExtra("id4",doctorProfileModel.getId());
                intent.putExtra("dName",doctorProfileModel.getDoctorName());
                intent.putExtra("detail",doctorProfileModel.getDoctorDetail());
                intent.putExtra("appMent",doctorProfileModel.getDoctorAppoinment());
                intent.putExtra("phone",doctorProfileModel.getDoctorPhone());
                intent.putExtra("email",doctorProfileModel.getDoctorEmail());
                startActivity(intent);
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.diet_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_diet) {
            Intent intent = new Intent(this,AddDoctorActivity.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }


}
