package com.example.afzal.icare;



import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;

import android.widget.TimePicker;
import android.widget.Toast;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import alarmService.DietReceiver;
import Database.DietDataResource;
import Database.DietModel;


public class AddDietActivity extends Activity {

    EditText dietMenuET,dietTypeET;

    CheckBox dietAlarmCB;
    String type,menu,date,time;
    boolean alarm;

    Calendar calendar1;
    EditText pickDate;
    int day, month, year;

    static final int TIME_DIALOG_ID = 1111;
    EditText timePicker;


    private int hour;
    private int minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_diet);
        dietTypeET = (EditText) findViewById(R.id.dietTypeET);
        dietMenuET = (EditText) findViewById(R.id.dietMenuET);
        dietAlarmCB = (CheckBox) findViewById(R.id.dietAlarmCB);

        pickDate = (EditText) findViewById(R.id.dietDateET);
        calendar1 = Calendar.getInstance();
        year = calendar1.get(Calendar.YEAR);
        month = calendar1.get(Calendar.MONTH);
        day = calendar1.get(Calendar.DAY_OF_MONTH);

        Date date1 = new Date();
        calendar1.setTime(date1);



        timePicker = (EditText) findViewById(R.id.dietTimeET);
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        addEditTextClickListener();
    }

    public void setDate(View view) {
        showDialog(999);

    }


    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        switch (id) {
            case TIME_DIALOG_ID:


                return new TimePickerDialog(this, timePickerListener, hour, minute,
                        false);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {

        pickDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    public void addEditTextClickListener() {


        timePicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

    }




    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {

            hour   = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);

        }

    };

    private void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);


        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        timePicker.setText(aTime);
    }

    public void dietSave (View v){
        type = dietTypeET.getText().toString();
        menu = dietMenuET.getText().toString();
        date = pickDate.getText().toString();
        time = timePicker.getText().toString();

        if (dietAlarmCB.isChecked()){
            alarm = true;
            AlarmManager alarmManager1 = (AlarmManager) getBaseContext().getSystemService(ALARM_SERVICE);

            Date date1 = new Date();
            calendar1.setTime(date1);
            calendar1.set(Calendar.MINUTE, 1);
            calendar1.set(Calendar.SECOND,1);

            Intent intent = new Intent(AddDietActivity.this, DietReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),0, intent,0);
            GregorianCalendar calendar = new GregorianCalendar(year,month,day, hour, minute);
            long alarm_time = calendar.getTimeInMillis();
            alarmManager1.set(AlarmManager.RTC_WAKEUP, alarm_time, pendingIntent);
            alarmManager1.setRepeating(AlarmManager.RTC_WAKEUP, alarm_time, 7 * 24 * 3600 * 1000, pendingIntent);


        }else {
            alarm = false;
        }

        DietDataResource dietDataResource = new DietDataResource(this);
        DietModel dietModel = new DietModel(type,menu,date,time,alarm);
        boolean inserted = dietDataResource.insert(dietModel);
        if (inserted){
            Toast.makeText(getApplicationContext(),"inserted",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,NextPartActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }else {
            Toast.makeText(getApplicationContext(),"insert failed",Toast.LENGTH_LONG).show();
        }
    }
}



