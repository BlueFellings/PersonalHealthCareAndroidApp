package com.example.afzal.icare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class CreateProfileActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_profile_screen);
    }
    public void createProfile(View v){
        Intent intent=new Intent(CreateProfileActivity.this,AddProfileActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

}
