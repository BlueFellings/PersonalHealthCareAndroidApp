package com.example.afzal.icare;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;




public class LoginActivity extends ActionBarActivity{

    EditText passwordET;
    String password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        passwordET= (EditText) findViewById(R.id.password);


        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF0A62AC")));



    }
    public void login(View v){

        password=passwordET.getText().toString();
//        UserDataResource userDataResource =new UserDataResource(this);
//        ArrayList<UserProfileModel>userProfileModels = userDataResource.getAllContact();

        if (password.equals("12345")) {
            Toast.makeText(getApplicationContext(), "login successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(LoginActivity.this, UserProfileList.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
            else if (password.equals("12335")){
                Intent intent = new Intent(LoginActivity.this, CreateProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }else {
            Toast.makeText(getApplicationContext(),"password incorrect",Toast.LENGTH_LONG).show();

        }
    }
//    public boolean isTableExits(DataBaseHelper dataBaseHelper){
//        if (dataBaseHelper==null) {
//            Intent intent = new Intent(LoginActivity.this, CreateProfileActivity.class);
//            startActivity(intent);
//
//        }else {
//            Intent intent=new Intent(LoginActivity.this,UserProfileList.class);
//            startActivity(intent);
//        }
//        return true;
//    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
       else if (id==R.id.general_info){
        Intent intent = new Intent(this,GeneralHealthInfo.class);
            startActivity(intent );

        }
       else if (id==R.id.exit){
            finish();
            System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }




}
