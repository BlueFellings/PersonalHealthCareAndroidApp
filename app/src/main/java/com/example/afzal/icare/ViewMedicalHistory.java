package com.example.afzal.icare;



import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import Adapter.MedicalAdapter;
import Database.DataBaseHelper;
import Database.MedicalDataResource;
import Database.MedicalHistoryModel;


public class ViewMedicalHistory extends ActionBarActivity {

    ListView medicalList;
    ArrayList<MedicalHistoryModel>medicalHistoryModels = new ArrayList<>();
    MedicalDataResource medicalDataResource;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medical_list);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF7BB159")));

        medicalList = (ListView) findViewById(R.id.medicalList);
        medicalDataResource = new MedicalDataResource(this);
        medicalHistoryModels = medicalDataResource.getAllContact();

        MedicalAdapter adapter = new MedicalAdapter(getApplicationContext(),medicalHistoryModels);
        medicalList.setAdapter(adapter);

        medicalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final MedicalHistoryModel medicalHistoryModel = medicalHistoryModels.get(position);

                final CharSequence[] items = { "Delete", "Edit", "Cancel" };
                AlertDialog.Builder builder = new AlertDialog.Builder(ViewMedicalHistory.this);
                builder.setTitle("Delete");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Delete")) {

                            medicalDataResource.deleteContact(medicalHistoryModel.getId());
                            Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(ViewMedicalHistory.this, NextPartActivity.class);
                            startActivity(intent);


                        } else if (items[item].equals("Edit")) {

                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }


        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.diet_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_diet) {
            Intent intent = new Intent(this,AddMedicalHistory.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }
}

