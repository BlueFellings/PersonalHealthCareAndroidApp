package com.example.afzal.icare;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import Adapter.VaccineAdapter;
import Database.VaccineDataResource;
import Database.VaccineModel;

public class VaccineHistoryActivity extends ActionBarActivity {
    ListView listView;
    VaccineDataResource vaccineDataResource;
    ArrayList<VaccineModel>allVaccineList=new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vaccine_list);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF7BB159")));

        listView= (ListView) findViewById(R.id.vaccineList);
        vaccineDataResource =new VaccineDataResource(this);
        allVaccineList=vaccineDataResource.getAllContact();

        VaccineAdapter adapter = new VaccineAdapter(getApplicationContext(),allVaccineList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final VaccineModel model = allVaccineList.get(position);

                final CharSequence[] items = { "Delete", "Edit", "Cancel" };
                AlertDialog.Builder builder = new AlertDialog.Builder(VaccineHistoryActivity.this);
                builder.setTitle("Delete");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Delete")) {

                            vaccineDataResource.deleteContact(model.getId());
                            Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(VaccineHistoryActivity.this,NextPartActivity.class);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                        } else if (items[item].equals("Edit")) {

                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.diet_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_diet) {
            Intent i = new Intent(VaccineHistoryActivity.this,AddVaccineActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
