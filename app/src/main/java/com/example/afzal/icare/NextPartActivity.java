package com.example.afzal.icare;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;




public class NextPartActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_part);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF0A62AC")));
    }
    public  void addDiet(View v){
        Intent i1=new Intent(getApplicationContext(),AddDietActivity.class);
        startActivity(i1);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void dietHistory(View v){
        Intent i2=new Intent(getApplicationContext(),DietHistoryActivity.class);
        startActivity(i2);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void addVaccine(View v){
        Intent i3=new Intent(getApplicationContext(),AddVaccineActivity.class);
        startActivity(i3);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void vaccineHistory(View v){
        Intent i4=new Intent(getApplicationContext(),VaccineHistoryActivity.class);
        startActivity(i4);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void adddoctor(View v){
        Intent i5=new Intent(getApplicationContext(),AddDoctorActivity.class);
        startActivity(i5);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void doctorHistory(View v){
        Intent i6=new Intent(getApplicationContext(),DoctorHistoryActivity.class);
        startActivity(i6);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void addMedical(View v){
        Intent i7=new Intent(getApplicationContext(),AddMedicalHistory.class);
        startActivity(i7);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public  void medicalHistory(View v){
        Intent i8=new Intent(getApplicationContext(),ViewMedicalHistory.class);
        startActivity(i8);overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.all_details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==R.id.all_general_info){
        Intent intent = new Intent(NextPartActivity.this,GeneralHealthInfo.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
       else if(id==R.id.medical_info){

        }
       else if (id==R.id.allLogOut){
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
        return super.onOptionsItemSelected(item);
    }
}


