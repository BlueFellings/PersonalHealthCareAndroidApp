package com.example.afzal.icare;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import Database.UserDataResource;
import Database.UserProfileModel;


public class ProfileDetailsActivity extends ActionBarActivity {

    TextView nameTV,ageTV,heightTV,weightTV,relationTV,bloodGroupTV;
    int profileId;
    String name,age,height,weight,relation,bloodGroup;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_profile);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF0A62AC")));

        nameTV = (TextView) findViewById(R.id.viewNameTV);
        ageTV = (TextView) findViewById(R.id.viewAgeTV);
        heightTV = (TextView) findViewById(R.id.viewHeightTV);
        weightTV = (TextView) findViewById(R.id.viewWeightTV);
        relationTV = (TextView) findViewById(R.id.viewRelationTV);
        bloodGroupTV = (TextView) findViewById(R.id.viewBloodTV);

        Intent intent = getIntent();
        profileId = intent.getIntExtra("id1", -1);
        name = intent.getStringExtra("name");
        age = intent.getStringExtra("age");
        height = intent.getStringExtra("height");
        weight = intent.getStringExtra("weight");
        relation = intent.getStringExtra("relation");
        bloodGroup = intent.getStringExtra("bloodGroup");

        nameTV.setText(name);
        ageTV.setText(age);
        heightTV.setText(height);
        weightTV.setText(weight);
        relationTV.setText(relation);
        bloodGroupTV.setText(bloodGroup);
    }
    public void nextPart(View v){
        Intent intent = new Intent(this,NextPartActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id==R.id.profile_edit){
            Intent intent = new Intent(this,ProfileEditActivity.class);
            intent.putExtra("id1",profileId);
            intent.putExtra("name",name);
            intent.putExtra("age",age);
            intent.putExtra("height",height);
            intent.putExtra("weight",weight);
            intent.putExtra("relation",relation);
            startActivity(intent);
        }
        if (id==R.id.profile_delete){
            UserDataResource dataResource = new UserDataResource(this);
           boolean delete = dataResource.deleteContact(profileId);
            if (delete){
                Toast.makeText(getApplicationContext(),"Profile Deleted",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),UserProfileList.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }else{
                Toast.makeText(getApplicationContext(),"Profile Delete Failed",Toast.LENGTH_LONG).show();
            }



        }


        return super.onOptionsItemSelected(item);
    }
}
