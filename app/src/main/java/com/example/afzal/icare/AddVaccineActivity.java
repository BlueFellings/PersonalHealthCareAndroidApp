package com.example.afzal.icare;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import alarmService.VaccineReceiver;
import Database.VaccineDataResource;
import Database.VaccineModel;


public class AddVaccineActivity extends Activity {

    EditText vaccineNameET,detailET;
    String vaccName, vDate,vTime,vDetail;
    VaccineDataResource vaccineDataResource;
    CheckBox vaccAlarmCB;
    boolean alarm;

    Calendar calendar1;
    EditText pickDate;
    int day, month, year;

    static final int TIME_DIALOG_ID = 1111;
    EditText timePicker;


    private int hour;
    private int minute;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_vaccination);
        vaccineNameET= (EditText) findViewById(R.id.vaccineNameET);
        detailET= (EditText) findViewById(R.id.VdetailET);
        vaccAlarmCB = (CheckBox) findViewById(R.id.vaccineCB);

        vaccineDataResource = new VaccineDataResource(this);

        pickDate= (EditText) findViewById(R.id.VdateET);
        calendar1 = Calendar.getInstance();
        year = calendar1.get(Calendar.YEAR);
        month = calendar1.get(Calendar.MONTH);
        day = calendar1.get(Calendar.DAY_OF_MONTH);



        timePicker= (EditText) findViewById(R.id.VtimeET);
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        addEditTextClickListener();

    }

    public void setDate(View view) {
        showDialog(999);

    }


    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        switch (id) {
            case TIME_DIALOG_ID:


                return new TimePickerDialog(this, timePickerListener, hour, minute,
                        false);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {

        pickDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    public void addEditTextClickListener() {


        timePicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

    }




    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {

            hour   = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);

        }

    };

    private void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);


        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        timePicker.setText(aTime);
    }


    public void addVac(View v){
    vaccName=vaccineNameET.getText().toString();
    vDate=pickDate.getText().toString();
    vTime=timePicker.getText().toString();
    vDetail=detailET.getText().toString();

        if (vaccAlarmCB.isChecked()){
            alarm = true;
            AlarmManager alarmManager1 = (AlarmManager) getBaseContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(AddVaccineActivity.this, VaccineReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent,0);
            Date date1 = new Date();
            calendar1.setTime(date1);
            calendar1.set(Calendar.HOUR_OF_DAY, hour);
            calendar1.set(Calendar.MINUTE,minute);


            GregorianCalendar calendar = new GregorianCalendar(year,month,day, hour, minute);
            long alarm_time = calendar.getTimeInMillis();
            alarmManager1.set(AlarmManager.RTC_WAKEUP  , alarm_time , pendingIntent);
            alarmManager1.setRepeating(AlarmManager.RTC_WAKEUP,alarm_time,7*24*3600*1000, pendingIntent);


        }else {
            alarm = false;
        }


    VaccineModel vaccineModel = new VaccineModel(vaccName,vDate,vTime,vDetail,alarm);
    boolean inserted = vaccineDataResource.insert(vaccineModel);

    if(inserted){
        Toast.makeText(getApplicationContext(), "Vaccine Inserted", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(AddVaccineActivity.this,NextPartActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }else{
        Toast.makeText(getApplicationContext(),"Vaccine Inserted failed",Toast.LENGTH_LONG).show();
    }


}


}
