package com.example.afzal.icare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import Database.UserDataResource;
import Database.UserProfileModel;


public class AddProfileActivity extends Activity {

    EditText nameET,ageET,heightET,weightET;
    Spinner relationSP,bloodGroupSP;
    String name,age,height,weight,relation,bloodGroup;
    UserDataResource userDataResource;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_profile);
        nameET = (EditText) findViewById(R.id.nameET);
        ageET = (EditText) findViewById(R.id.ageET);
        heightET = (EditText) findViewById(R.id.heightET);
        weightET = (EditText) findViewById(R.id.weightET);
        userDataResource = new UserDataResource(this);
        relationSP = (Spinner) findViewById(R.id.relationSpinner);
        bloodGroupSP = (Spinner) findViewById(R.id.bloodSpinner);

    }

    public  void addProfile(View v){

        age = ageET.getText().toString();
        height = heightET.getText().toString();
        weight = weightET.getText().toString();
        relation = relationSP.getSelectedItem().toString();
        bloodGroup = bloodGroupSP.getSelectedItem().toString();

        if (nameET!=null){
            name = nameET.getText().toString();
        }else {
            Toast.makeText(getApplicationContext(),"enter name",Toast.LENGTH_LONG).show();
        }


        UserProfileModel userProfileModel = new UserProfileModel(name,age,height,weight,relation,bloodGroup);

        boolean inserted = userDataResource.insert(userProfileModel);
        if (inserted){
            Toast.makeText(getApplicationContext(), "inserted", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(AddProfileActivity.this,UserProfileList.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }else{
            Toast.makeText(getApplicationContext(),"inserted failed",Toast.LENGTH_LONG).show();
        }

    }

}
