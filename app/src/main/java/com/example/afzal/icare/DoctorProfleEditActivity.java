package com.example.afzal.icare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import Database.DoctorDataResource;
import Database.DoctorProfileModel;
import Database.UserDataResource;


public class DoctorProfleEditActivity extends Activity {
    EditText namesET,detailsET,appMentET,phonesET,emailsET;

    String names,details,appMent,phones,emails;
    DoctorDataResource dataResource;
    int profileId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_edit);
        namesET = (EditText) findViewById(R.id.DeditNameET);
        detailsET = (EditText) findViewById(R.id.doctorEditDetailsET);
        appMentET = (EditText) findViewById(R.id.editAppMenttET);
        phonesET = (EditText) findViewById(R.id.dEditPhoneET);
        emailsET = (EditText) findViewById(R.id.dEditEmailET);
        dataResource = new DoctorDataResource(this);

        Intent intent = getIntent();
        profileId = intent.getIntExtra("id44", -1);
        Log.d("tagmaster","ProfileId:: "+ String.valueOf(profileId));
        names = intent.getStringExtra("ddname");
        details = intent.getStringExtra("ddetail");
        appMent = intent.getStringExtra("dappMent");
        phones = intent.getStringExtra("dphone");
        emails = intent.getStringExtra("demail");

        namesET.setText(names);
        detailsET.setText(details);
        appMentET.setText(appMent);
        phonesET.setText(phones);
        emailsET.setText(emails);



    }
    public void update (View v){
        DoctorProfileModel doctorProfileModel = new DoctorProfileModel(names,details,appMent,phones,emails);
        boolean updated = dataResource.updateContact(profileId,doctorProfileModel);
        if (updated){
            Toast.makeText(getApplicationContext(), "updated", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,DoctorHistoryActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(),"not update",Toast.LENGTH_LONG).show();
        }

    }

}


