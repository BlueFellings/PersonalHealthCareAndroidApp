package com.example.afzal.icare;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import Database.DoctorDataResource;
import Database.UserDataResource;


public class DoctorProfileDetailsActivity extends ActionBarActivity {

    TextView docNameTV,docDetailTV,docAppoinmentTV,docPhoneTV,docEmailTV;
    int profileId;
    String name,details,appoinment,phone,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_doctor_profile);
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF0A62AC")));

        docNameTV = (TextView) findViewById(R.id.viewDnameTV);
        docDetailTV = (TextView) findViewById(R.id.viewDetailTV);
        docAppoinmentTV = (TextView) findViewById(R.id.viewAppMentTV);
        docPhoneTV = (TextView) findViewById(R.id.viewPhoneTV);
        docEmailTV = (TextView) findViewById(R.id.viewEmailTV);


        Intent intent = getIntent();
        profileId = intent.getIntExtra("id4", -1);
        name = intent.getStringExtra("dName");
        details = intent.getStringExtra("detail");
        appoinment = intent.getStringExtra("appMent");
        phone = intent.getStringExtra("phone");
        email = intent.getStringExtra("email");

        docNameTV.setText(name);
        docDetailTV.setText(details);
        docAppoinmentTV.setText(appoinment);
        docPhoneTV.setText(phone);
        docEmailTV.setText(email);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.doctor_profile_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id==R.id.doctor_profile_edit){
            Intent intent = new Intent(this,DoctorProfleEditActivity.class);
            intent.putExtra("id44",profileId);
            intent.putExtra("ddname",name);
            intent.putExtra("ddetail",details);
            intent.putExtra("dappMent",appoinment);
            intent.putExtra("dphone",phone);
            intent.putExtra("demail",email);
            startActivity(intent);
        }
        if (id==R.id.doctor_profile_delete){
            DoctorDataResource dataResource = new DoctorDataResource(this);
            boolean delete = dataResource.deleteContact(profileId);
            if (delete){
                Toast.makeText(getApplicationContext(), "Profile Deleted", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),NextPartActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }else{
                Toast.makeText(getApplicationContext(),"Profile Delete Failed",Toast.LENGTH_LONG).show();
            }



        }

        if (id==R.id.logOut){
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
