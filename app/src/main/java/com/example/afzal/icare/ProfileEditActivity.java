package com.example.afzal.icare;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;

import android.widget.EditText;
import android.widget.Toast;

import Database.UserDataResource;


public class ProfileEditActivity extends Activity {
    EditText nameET,ageET,heightET,weightET;
    String name,age,height,weight,relation;
    UserDataResource userDataResource;
    int profileId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.porfile_edit);
        nameET = (EditText) findViewById(R.id.EnameET);
        ageET = (EditText) findViewById(R.id.EageET);
        heightET = (EditText) findViewById(R.id.EheightET);
        weightET = (EditText) findViewById(R.id.EweightET);
        userDataResource = new UserDataResource(this);

        Intent intent = getIntent();
        profileId = intent.getIntExtra("id1", -1);
        name = intent.getStringExtra("name");
        age = intent.getStringExtra("age");
        height = intent.getStringExtra("height");
        weight = intent.getStringExtra("weight");
        relation = intent.getStringExtra("relation");

        nameET.setText(name);
        ageET.setText(age);
        heightET.setText(height);
        weightET.setText(weight);



    }
    public void update (View v){
        boolean updated = userDataResource.update(profileId);
        if (updated){
            Toast.makeText(getApplicationContext(),"updated",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,UserProfileList.class);
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(),"not update",Toast.LENGTH_LONG).show();
        }

    }
}
