package com.example.afzal.icare;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class GeneralHealthInfo extends Activity {

    ListView listView;

    String[] generalinfo = {"1. Curb your sweet tooth.\n\n  Got a late-night sugar craving that just won't quit?To satisfy your sweet tooth without pushing yourself over the calorie edge, even in the late night hours, think fruit first,says Jackie Newgent, RD, author of The Big Green Cookbook. ",
            "2.Tweak your lifestyle.\n\n  It's a familiar story: You pledge to honor a daily elliptical routine and count every last calorie. But soon, you're eating cupcakes at the office and grabbing happy hour mojitos, thinking, Oops, diet over.",
            "3. Daily exercise.\n\n  You brush your teeth every day; exercise is equally important for your daily routine. Turn off the TV or computer, and get at least 30 minutes of exercise every day.",
            "4. Less stress.\n\n  When a person says they're too busy to exercise, it tells me other things are crowding out what's important in life: They don't spend time with family and friends; don't exercise enough; don't eat right; don't sleep properly. All these things reduce stress in your life, and that is critical to your health and longevity.",
            "5. Regular physical exams.\n\n  Tell your doctor your family medical history. Learn your personal risk factors, and the screening tests you need. Women may have mammograms to screen for breast cancer and Pap tests for cervical cancer. Men may have prostate cancer PSA tests. Routine screening for colorectal cancer should start at age 50, perhaps earlier if colon cancer runs in your family. You also need regular diabetes, blood pressure, and cholesterol tests. Make sure your immunizations are up to date. You may need flu and pneumonia shots, depending on your age."};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generalhealth_listview);

        ArrayAdapter adapter = new ArrayAdapter<>(this, R.layout.generaltextview, generalinfo);

        listView = (ListView) findViewById(R.id.general_information);
        listView.setAdapter(adapter);

    }

}
