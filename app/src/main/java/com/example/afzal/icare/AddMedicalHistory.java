package com.example.afzal.icare;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import Database.DoctorDataResource;
import Database.DoctorProfileModel;
import Database.MedicalDataResource;
import Database.MedicalHistoryModel;


public class AddMedicalHistory extends Activity{
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int SELECT_FILE = 1;
    ImageView imgPreview;
    Uri fileUri;

    Calendar calendar1;
    EditText pickDate;
    int day, month, year;

    ArrayList<DoctorProfileModel> singleName = new ArrayList<>();
    Spinner medicalSpinner;

    EditText dateET,detailsET;
    String date,details,doctorName;
    byte[]byteArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_medical_history);
        medicalSpinner = (Spinner) findViewById(R.id.medicalSpinner);
        dateET = (EditText) findViewById(R.id.mDateET);
        detailsET = (EditText) findViewById(R.id.mDetailsET);

        imgPreview = (ImageView) findViewById(R.id.imagePreview);
        pickDate = (EditText) findViewById(R.id.mDateET);

        calendar1 = Calendar.getInstance();
        year = calendar1.get(Calendar.YEAR);
        month = calendar1.get(Calendar.MONTH);
        day = calendar1.get(Calendar.DAY_OF_MONTH);

        DoctorDataResource doctorDataResource = new DoctorDataResource(this);

        singleName = doctorDataResource.getAllContact();
        ArrayList<String>name = new ArrayList<>();
        for (DoctorProfileModel doctorProfileModel : singleName){
            name.add(doctorProfileModel.getDoctorName());
        }
        ArrayAdapter<String>adapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,name);
        medicalSpinner.setAdapter(adapter);




    }
    public void setDate(View view) {
        showDialog(999);

    }


    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return  null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {

        pickDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    public void takeImage(View v) {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(AddMedicalHistory.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE||requestCode==SELECT_FILE) {
            if (resultCode == RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imgPreview.setImageBitmap(photo);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArray = stream.toByteArray();

            } else if (resultCode == RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {

                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

public void addMedicalHistory(View v){
    date = dateET.getText().toString();
    details = detailsET.getText().toString();
    doctorName = medicalSpinner.getSelectedItem().toString();

    MedicalDataResource medicalDataResource = new MedicalDataResource(this);
    MedicalHistoryModel medicalHistoryModel = new MedicalHistoryModel(byteArray,doctorName,details,date);
    boolean inserted = medicalDataResource.insert(medicalHistoryModel);
    if (inserted){
        Toast.makeText(getApplicationContext(),"inserted",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,NextPartActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }else {
        Toast.makeText(getApplicationContext(),"insert failed",Toast.LENGTH_LONG).show();
    }

}

}
