package Database;


public class MedicalHistoryModel {
    private int id;
    private byte[] image;
    private String medicalDoctorName;
    private String medicalDetail;
    private String medicalDate;

    public MedicalHistoryModel(int id, byte[] image, String medicalDoctorName, String medicalDetail, String medicalDate) {
        this.id = id;
        this.image = image;
        this.medicalDoctorName = medicalDoctorName;
        this.medicalDetail = medicalDetail;
        this.medicalDate = medicalDate;
    }

    public MedicalHistoryModel(byte[] image, String medicalDoctorName, String medicalDetail, String medicalDate) {
        this.image = image;
        this.medicalDoctorName = medicalDoctorName;
        this.medicalDetail = medicalDetail;
        this.medicalDate = medicalDate;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getMedicalDoctorName() {
        return medicalDoctorName;
    }

    public void setMedicalDoctorName(String medicalDoctorName) {
        this.medicalDoctorName = medicalDoctorName;
    }

    public String getMedicalDetail() {
        return medicalDetail;
    }

    public void setMedicalDetail(String medicalDetail) {
        this.medicalDetail = medicalDetail;
    }

    public String getMedicalDate() {
        return medicalDate;
    }

    public void setMedicalDate(String medicalDate) {
        this.medicalDate = medicalDate;
    }

    public int getId() {
        return id;
    }
}
