package Database;


public class DoctorProfileModel {
    private int id;
    private String doctorName;

    public DoctorProfileModel(int id, String doctorName) {
        this.id = id;
        this.doctorName = doctorName;
    }

    private String doctorDetail;
    private String doctorAppoinment;
    private String doctorPhone;
    private String doctorEmail;

    public DoctorProfileModel(int id, String doctorName, String doctorDetail, String doctorAppoinment, String doctorPhone, String doctorEmail) {
        this.id = id;
        this.doctorName = doctorName;
        this.doctorDetail = doctorDetail;
        this.doctorAppoinment = doctorAppoinment;
        this.doctorPhone = doctorPhone;
        this.doctorEmail = doctorEmail;
    }

    public DoctorProfileModel(String doctorName, String doctorDetail, String doctorAppoinment, String doctorPhone, String doctorEmail) {
        this.doctorName = doctorName;
        this.doctorDetail = doctorDetail;
        this.doctorAppoinment = doctorAppoinment;
        this.doctorPhone = doctorPhone;
        this.doctorEmail = doctorEmail;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorDetail() {
        return doctorDetail;
    }

    public void setDoctorDetail(String doctorDetail) {
        this.doctorDetail = doctorDetail;
    }

    public String getDoctorAppoinment() {
        return doctorAppoinment;
    }

    public void setDoctorAppoinment(String doctorAppoinment) {
        this.doctorAppoinment = doctorAppoinment;
    }

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }

    public String getDoctorEmail() {
        return doctorEmail;
    }

    public void setDoctorEmail(String doctorEmail) {
        this.doctorEmail = doctorEmail;
    }

    public int getId() {
        return id;
    }
}
