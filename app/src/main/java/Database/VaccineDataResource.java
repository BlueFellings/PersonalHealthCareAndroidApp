package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class VaccineDataResource {

    DataBaseHelper helper;
    VaccineModel vaccineModel;
    SQLiteDatabase database;

    public VaccineDataResource(Context context) {
        helper=new DataBaseHelper(context);
    }

    private void open(){
        database=helper.getWritableDatabase();
    }
    private void close(){
        helper.close();
    }

    //insert data
    public boolean insert(VaccineModel vaccineModel){
        this.open();
        ContentValues values=new ContentValues();
        values.put(DataBaseHelper.COL_VACC_NAME,vaccineModel.getVaccineName());
        values.put(DataBaseHelper.COL_VACC_DATE,vaccineModel.getVaccineDate());
        values.put(DataBaseHelper.COL_VACC_TIME,vaccineModel.getVaccineTime());
        values.put(DataBaseHelper.COL_VACC_DETAILS,vaccineModel.getVaccineDetails());
        values.put(String.valueOf(DataBaseHelper.COL_VACC_ALARM),vaccineModel.isAlarm());

        long inserted=database.insert(DataBaseHelper.VACCINE_TABLE_NAME,null,values);
        if (inserted>0){
            return true;
        }
        return false;
    }
    // contact list
    public ArrayList<VaccineModel> getAllContact () {

        database = helper.getReadableDatabase();
        ArrayList<VaccineModel> contactList = new ArrayList<>();

        Cursor cursor = database.query(DataBaseHelper.VACCINE_TABLE_NAME, null, null, null, null, null, null) ;
        if (cursor != null) {
            cursor.moveToFirst();

            for (int i = 0 ; i < cursor.getCount(); i++ ) {
                int id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COL_VACC_ID));
                String vaccineName = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_VACC_NAME));
                String vaccineDate = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_VACC_DATE));
                String vaccineTime = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_VACC_TIME));
                String vaccineDetails = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_VACC_DETAILS));
                boolean alarm = cursor.isNull(cursor.getColumnIndex(String.valueOf(DataBaseHelper.COL_VACC_ALARM)));

                vaccineModel = new VaccineModel(id,vaccineName,vaccineDate,vaccineTime,vaccineDetails,alarm);

                contactList.add(vaccineModel);

                cursor.moveToNext();
            }



        }
        return contactList;
    }

    // delete contact
    public void deleteContact (int id){
        database =helper.getWritableDatabase() ;
        String whereClause = DataBaseHelper.COL_VACC_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.delete(DataBaseHelper.VACCINE_TABLE_NAME, whereClause, whereArgs );
        database.close();
    }

    public void updateContact (int id){
        database = helper.getReadableDatabase();
        String whereClause = DataBaseHelper.COL_VACC_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.update(DataBaseHelper.VACCINE_TABLE_NAME, null, whereClause, whereArgs );
        database.close();
    }

}



