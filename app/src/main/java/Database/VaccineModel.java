package Database;


public class VaccineModel {

    private int id;
    private String vaccineName;
    private String vaccineDate;
    private String vaccineTime;
    private String vaccineDetails;
    private boolean alarm;



    public VaccineModel(int id, String vaccineName, String vaccineDate, String vaccineTime, String vaccineDetails,boolean alarm) {
        this.id = id;
        this.vaccineName = vaccineName;
        this.vaccineDate = vaccineDate;
        this.vaccineTime = vaccineTime;
        this.vaccineDetails = vaccineDetails;
        this.alarm = alarm;
    }

    public VaccineModel(String vaccineName, String vaccineDate, String vaccineTime, String vaccineDetails,boolean alarm) {
        this.vaccineName = vaccineName;
        this.vaccineDate = vaccineDate;
        this.vaccineTime = vaccineTime;
        this.vaccineDetails = vaccineDetails;
        this.alarm = alarm;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getVaccineDate() {
        return vaccineDate;
    }

    public void setVaccineDate(String vaccineDate) {
        this.vaccineDate = vaccineDate;
    }

    public String getVaccineTime() {
        return vaccineTime;
    }

    public void setVaccineTime(String vaccineTime) {
        this.vaccineTime = vaccineTime;
    }

    public String getVaccineDetails() {
        return vaccineDetails;
    }

    public void setVaccineDetails(String vaccineDetails) {
        this.vaccineDetails = vaccineDetails;
    }
    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public int getId() {
        return id;
    }
}
