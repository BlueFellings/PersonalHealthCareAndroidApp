package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;


public class DoctorDataResource {

    DataBaseHelper helper;
    DoctorProfileModel doctorProfileModel;
    SQLiteDatabase database;

    public DoctorDataResource(Context context) {
        helper=new DataBaseHelper(context);
    }

    private void open(){
        database=helper.getWritableDatabase();
    }
    private void close(){
        helper.close();
    }

    //insert data
    public boolean insert(DoctorProfileModel doctorProfileModel){
        this.open();
        ContentValues values=new ContentValues();
        values.put(DataBaseHelper.COL_DOC_NAME,doctorProfileModel.getDoctorName());
        values.put(DataBaseHelper.COL_DOC_DETAILS,doctorProfileModel.getDoctorDetail());
        values.put(DataBaseHelper.COL_DOC_APPOINMENT,doctorProfileModel.getDoctorAppoinment());
        values.put(DataBaseHelper.COL_DOC_PHONE,doctorProfileModel.getDoctorPhone());
        values.put(DataBaseHelper.COL_DOC_EMAIL,doctorProfileModel.getDoctorEmail());

        long inserted=database.insert(DataBaseHelper.DOC_TABLE_NAME, null, values);
        if (inserted>0){
            return true;
        }
        return false;
    }
    // contact list
    public ArrayList<DoctorProfileModel> getAllContact () {

        database = helper.getReadableDatabase();
        ArrayList<DoctorProfileModel> contactList = new ArrayList<>();

        Cursor cursor = database.query(DataBaseHelper.DOC_TABLE_NAME, null, null, null, null, null, null) ;
        if (cursor != null) {
            cursor.moveToFirst();

            for (int i = 0 ; i < cursor.getCount(); i++ ) {
                int id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COL_DOC_ID));
                String doctorName = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DOC_NAME));
                String doctorDetails = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DOC_DETAILS));
                String doctorAppoinment = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DOC_APPOINMENT));
                String doctorPhone = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DOC_PHONE));
                String doctorEmail = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DOC_EMAIL));

                doctorProfileModel = new DoctorProfileModel(id,doctorName,doctorDetails,doctorAppoinment,doctorPhone,doctorEmail);
                contactList.add(doctorProfileModel);

                cursor.moveToNext();
            }

        }
        return contactList;
    }

    // delete contact
    public boolean deleteContact(int id){
        database =helper.getWritableDatabase() ;
        String whereClause = DataBaseHelper.COL_DOC_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.delete(DataBaseHelper.DOC_TABLE_NAME, whereClause, whereArgs);
        database.close();
        return true;
    }

    public boolean updateContact(int id,DoctorProfileModel doctorProfileModel){
        this.open();
        database = helper.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(DataBaseHelper.COL_DOC_NAME,doctorProfileModel.getDoctorName());
        values.put(DataBaseHelper.COL_DOC_DETAILS,doctorProfileModel.getDoctorDetail());
        values.put(DataBaseHelper.COL_DOC_APPOINMENT,doctorProfileModel.getDoctorAppoinment());
        values.put(DataBaseHelper.COL_DOC_PHONE,doctorProfileModel.getDoctorPhone());
        values.put(DataBaseHelper.COL_DOC_EMAIL, doctorProfileModel.getDoctorEmail());
        String whereClause = DataBaseHelper.COL_DOC_ID + "=?";
        String whereArgs[] = {String.valueOf(id)};
        Log.d("tagmaster", whereArgs[0]);

        int s = database.update(DataBaseHelper.DOC_TABLE_NAME, values, whereClause, whereArgs);
        database.close();
        Log.d("tagmaster", "Input result: "+String.valueOf(s));
        return true;
    }

}






