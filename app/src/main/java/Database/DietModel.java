package Database;


public class DietModel {

    private int id;
    private String type;
    private String menu;
    private String date;
    private String time;
    private boolean alarm;


    public DietModel(int id, String type, String menu, String date, String time, boolean alarm) {
        this.id = id;
        this.type = type;
        this.menu = menu;
        this.date = date;
        this.time = time;
        this.alarm = alarm;

    }

    public DietModel(String type, String menu, String date, String time, boolean alarm) {
        this.type = type;
        this.menu = menu;
        this.date = date;
        this.time = time;
        this.alarm = alarm;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public int getId() {
        return id;
    }
}
