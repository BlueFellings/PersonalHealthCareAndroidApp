package Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME="Icare";
    public static final int DATABASE_VERSION=1;

    public static final String PROFILE_TABLE_NAME="profile_table";
    public static final String COL_ID="id1";
    public static final String COL_NAME="name";
    public static final String COL_AGE="age";
    public static final String COL_HEIGHT="height";
    public static final String COL_WEIGHT="weight";
    public static final String COL_RELATION="relation";
    public static final String COL_BLOODGROUP="bloodgroup";

    public static final String PROFILE_TABLE_CREATE=" CREATE TABLE "+PROFILE_TABLE_NAME + "( " +COL_ID +" INTEGER PRIMARY KEY," + COL_NAME +" TEXT NOT NULL, " + COL_AGE +" TEXT NOT NULL, "+COL_HEIGHT+" TEXT NOT NULL, "
            +COL_WEIGHT+" TEXT NOT NULL, "+COL_RELATION+" TEXT NOT NULL, "+COL_BLOODGROUP+" TEXT NOT NULL  )";

    public static final String DIET_TABLE_NAME="diet_table";
    public static final String COL_DIET_ID="id2";
    public static final String COL_DIET_TYPE="type";
    public static final String COL_DIET_MENU="menu";
    public static final String COL_DIET_DATE="date";
    public static final String COL_DIET_TIME="time";
    public static final boolean COL_DIET_ALARM=true;


    public static final String DIET_TABLE_CREATE=" CREATE TABLE "+DIET_TABLE_NAME + "( " +COL_DIET_ID +" INTEGER PRIMARY KEY," + COL_DIET_TYPE +" TEXT NOT NULL, " + COL_DIET_MENU +" TEXT NOT NULL, "+COL_DIET_DATE+" TEXT NOT NULL, "
            +COL_DIET_TIME+" TEXT NOT NULL, "+ COL_DIET_ALARM +" BOOLEAN   )";

    public static final String VACCINE_TABLE_NAME="vaccine_table";
    public static final String COL_VACC_ID="id3";
    public static final String COL_VACC_NAME="vaccine_name";
    public static final String COL_VACC_DATE="vaccine_date";
    public static final String COL_VACC_TIME="vaccine_time";
    public static final String COL_VACC_DETAILS="vaccine_details";
    public static final boolean COL_VACC_ALARM=true;

    public static final String VACCINE_TABLE_CREATE=" CREATE TABLE "+VACCINE_TABLE_NAME + "( " +COL_VACC_ID +" INTEGER PRIMARY KEY," + COL_VACC_NAME +" TEXT NOT NULL, " + COL_VACC_DATE +" TEXT NOT NULL, "+COL_VACC_TIME+" TEXT NOT NULL, "
            +COL_VACC_DETAILS+" TEXT NOT NULL, " + COL_VACC_ALARM +" BLOOEAN )";

    public static final String DOC_TABLE_NAME="doctor_table";
    public static final String COL_DOC_ID="id4";
    public static final String COL_DOC_NAME="doctor_name";
    public static final String COL_DOC_DETAILS="doctor_details";
    public static final String COL_DOC_APPOINMENT="doctor_appoinment";
    public static final String COL_DOC_PHONE="doctor_phone";
    public static final String COL_DOC_EMAIL="doctor_email";

    public static final String DOCTOR_TABLE_CREATE=" CREATE TABLE "+DOC_TABLE_NAME + "( " +COL_DOC_ID +" INTEGER PRIMARY KEY," + COL_DOC_NAME +" TEXT NOT NULL, " + COL_DOC_DETAILS +" TEXT NOT NULL, "+COL_DOC_APPOINMENT+" TEXT NOT NULL, "
            +COL_DOC_PHONE+" TEXT NOT NULL, "+COL_DOC_EMAIL+" TEXT NOT NULL  )";

    public static final String MED_TABLE_NAME="medical_table";
    public static final String COL_MED_ID="id5";
    public static final String COL_MED_IMAGE="medical_image";
    public static final String COL_MED_DOC_NAME="medical_doctor_name";
    public static final String COL_MED_DETAILS="medical_doctor_details";
    public static final String COL_MED_DATE="medical_date";

        public static final String MEDICAL_TABLE_CREATE=" CREATE TABLE "+MED_TABLE_NAME + "( " +COL_MED_ID +" INTEGER PRIMARY KEY," + COL_MED_IMAGE +" BLOB, " + COL_MED_DOC_NAME +" TEXT, "+COL_MED_DETAILS+" TEXT NOT NULL, "
            +COL_MED_DATE+" TEXT )";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PROFILE_TABLE_CREATE);
        db.execSQL(DIET_TABLE_CREATE);
        db.execSQL(VACCINE_TABLE_CREATE);
        db.execSQL(DOCTOR_TABLE_CREATE);
        db.execSQL(MEDICAL_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       db.execSQL("DROPS TABLE IF EXISTS"+PROFILE_TABLE_NAME);
        db.execSQL("DROPS TABLE IF EXISTS"+DIET_TABLE_NAME);
        db.execSQL("DROPS TABLE IF EXISTS"+VACCINE_TABLE_NAME);
        db.execSQL("DROPS TABLE IF EXISTS"+DOC_TABLE_NAME);
        db.execSQL("DROPS TABLE IF EXISTS"+MED_TABLE_NAME);
         onCreate(db);
    }
}
