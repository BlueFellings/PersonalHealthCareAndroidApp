package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class UserDataResource {

        DataBaseHelper helper;
        UserProfileModel userProfileModel;
        SQLiteDatabase database;

        public UserDataResource(Context context) {
            helper=new DataBaseHelper(context);
        }

        private void open(){
            database=helper.getWritableDatabase();
        }
        private void close(){
            helper.close();
        }

                    //insert data
        public boolean insert(UserProfileModel userProfileModel){
            this.open();
            ContentValues values=new ContentValues();
            values.put(DataBaseHelper.COL_NAME,userProfileModel.getName());
            values.put(DataBaseHelper.COL_AGE,userProfileModel.getAge());
            values.put(DataBaseHelper.COL_HEIGHT,userProfileModel.getHeight());
            values.put(DataBaseHelper.COL_WEIGHT,userProfileModel.getWeight());
            values.put(DataBaseHelper.COL_RELATION,userProfileModel.getRelation());
            values.put(DataBaseHelper.COL_BLOODGROUP,userProfileModel.getBloodGroup());


            long inserted=database.insert(DataBaseHelper.PROFILE_TABLE_NAME,null,values);
            if (inserted>0){
                return true;
            }
            return false;
        }
        // contact list
        public ArrayList<UserProfileModel> getAllContact () {

            database = helper.getReadableDatabase();
            ArrayList<UserProfileModel> contactList = new ArrayList<>();

            Cursor cursor = database.query(DataBaseHelper.PROFILE_TABLE_NAME, null, null, null, null, null, null) ;
            if (cursor != null) {
                cursor.moveToFirst();

                for (int i = 0 ; i < cursor.getCount(); i++ ) {
                    int id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COL_ID));
                    String name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_NAME));
                    String age = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_AGE));
                    String height = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_HEIGHT));
                    String weight = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_WEIGHT));
                    String relation = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_RELATION));
                    String bloodGroup = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_BLOODGROUP));

                    userProfileModel = new UserProfileModel(id,name,age,height,weight,relation,bloodGroup);

                    contactList.add(userProfileModel);

                    cursor.moveToNext();
                }



            }
            return contactList;
        }

        // delete contact
        public boolean deleteContact(int id){
            database =helper.getWritableDatabase() ;
            String whereClause = DataBaseHelper.COL_ID + " = ?";
            String whereArgs[] = {String.valueOf(id)};
            database.delete(DataBaseHelper.PROFILE_TABLE_NAME, whereClause, whereArgs );
            database.close();
            return true;
        }

//    public boolean update(int id, UserProfileModel userProfileModel) {
//        this.open();
//
//        ContentValues cv = new ContentValues();
//        cv.put(DataBaseHelper.COL_NAME, userProfileModel.getName());
//        cv.put(DataBaseHelper.COL_AGE, userProfileModel.getAge());
//        cv.put(DataBaseHelper.COL_HEIGHT, userProfileModel.getHeight());
//        cv.put(DataBaseHelper.COL_WEIGHT, userProfileModel.getWeight());
//
//        int updated = database.update(DataBaseHelper.PROFILE_TABLE_NAME, cv, DataBaseHelper.COL_ID + "= " + id, null);
//
//        this.close();
//
//        if (updated > 0) {
//            return true;
//        } else {
//            return false;
//        }
//
//
//    }



    public boolean update(int id){
            database = helper.getWritableDatabase();
            String whereClause = DataBaseHelper.COL_ID + " = ?";
            String whereArgs[] = {String.valueOf(id)};

            database.update(DataBaseHelper.PROFILE_TABLE_NAME,null,whereClause,whereArgs);
            database.close();
        return true;
    }

}


