package Database;


public class UserProfileModel {
    private int id;
    private String name;
    private String age;
    private String height;
    private String weight;
    private String relation;
    private String bloodGroup;



    public UserProfileModel(int id, String name,String age, String height, String weight, String relation,String bloodGroup) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.relation = relation;
        this.bloodGroup = bloodGroup;
    }

    public UserProfileModel(String name,String age, String height, String weight, String relation,String bloodGroup) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.relation = relation;
        this.bloodGroup = bloodGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }
    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public int getId() {
        return id;
    }
}
