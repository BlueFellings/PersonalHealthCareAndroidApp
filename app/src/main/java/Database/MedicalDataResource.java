package Database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class MedicalDataResource {

    DataBaseHelper helper;
    MedicalHistoryModel medical;
    SQLiteDatabase database;

    public MedicalDataResource(Context context) {
        helper=new DataBaseHelper(context);
    }

    private void open(){
        database=helper.getWritableDatabase();
    }
    private void close(){
        helper.close();
    }

    //insert data
    public boolean insert(MedicalHistoryModel medical){
        this.open();
        ContentValues values=new ContentValues();
        values.put(DataBaseHelper.COL_MED_IMAGE,medical.getImage());
        values.put(DataBaseHelper.COL_MED_DOC_NAME,medical.getMedicalDoctorName());
        values.put(DataBaseHelper.COL_MED_DETAILS,medical.getMedicalDetail());
        values.put(DataBaseHelper.COL_MED_DATE,medical.getMedicalDate());

        long inserted=database.insert(DataBaseHelper.MED_TABLE_NAME,null,values);
        if (inserted>0){
            return true;
        }
        return false;
    }
    // contact list
    public ArrayList<MedicalHistoryModel> getAllContact () {

        database = helper.getReadableDatabase();
        ArrayList<MedicalHistoryModel> contactList = new ArrayList<>();

        Cursor cursor = database.query(DataBaseHelper.MED_TABLE_NAME, null, null, null, null, null, null) ;
        if (cursor != null) {
            cursor.moveToFirst();

            for (int i = 0 ; i < cursor.getCount(); i++ ) {
                int id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COL_MED_ID));
                byte [] image = cursor.getBlob(1);
                String medicalDoctorName = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_MED_DOC_NAME));
                String medicalDetails = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_MED_DETAILS));
                String medicalDate = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_MED_DATE));

                medical = new MedicalHistoryModel(id,image,medicalDoctorName,medicalDetails,medicalDate);

                contactList.add(medical);

                cursor.moveToNext();
            }



        }
        return contactList;
    }

    // delete contact
    public boolean deleteContact(int id){
        database =helper.getWritableDatabase() ;
        String whereClause = DataBaseHelper.COL_MED_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.delete(DataBaseHelper.MED_TABLE_NAME, whereClause, whereArgs );
        database.close();
        return false;
    }

    public boolean updateContact (int id){
        database = helper.getWritableDatabase();
        String whereClause = DataBaseHelper.COL_MED_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.update(DataBaseHelper.MED_TABLE_NAME, null, whereClause, whereArgs);
        database.close();
        return true;
    }

}



