package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class DietDataResource {

    DataBaseHelper helper;
    DietModel dietModel;
    SQLiteDatabase database;

    public DietDataResource(Context context) {
        helper=new DataBaseHelper(context);
    }

    private void open(){
        database=helper.getWritableDatabase();
    }
    private void close(){
        helper.close();
    }

    //insert data
    public boolean insert(DietModel dietModel){
        this.open();
        ContentValues values=new ContentValues();
        values.put(DataBaseHelper.COL_DIET_TYPE,dietModel.getType());
        values.put(DataBaseHelper.COL_DIET_MENU,dietModel.getMenu());
        values.put(DataBaseHelper.COL_DIET_DATE,dietModel.getDate());
        values.put(DataBaseHelper.COL_DIET_TIME,dietModel.getTime());
        values.put(String.valueOf(DataBaseHelper.COL_DIET_ALARM),dietModel.isAlarm());


        long inserted=database.insert(DataBaseHelper.DIET_TABLE_NAME,null,values);
        if (inserted>0){
            return true;
        }
        return false;
    }
    // contact list
    public ArrayList<DietModel> getAllContact () {

        database = helper.getReadableDatabase();
        ArrayList<DietModel> contactList = new ArrayList<>();

        Cursor cursor = database.query(DataBaseHelper.DIET_TABLE_NAME, null, null, null, null, null, null) ;
        if (cursor != null) {
            cursor.moveToFirst();

            for (int i = 0 ; i < cursor.getCount(); i++ ) {
                int id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COL_DIET_ID));
                String type = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DIET_TYPE));
                String menu = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DIET_MENU));
                String date = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DIET_DATE));
                String time = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COL_DIET_TIME));
                boolean alarm = cursor.isNull(cursor.getColumnIndex(String.valueOf(DataBaseHelper.COL_DIET_ALARM)));


                dietModel = new DietModel(id,type,menu,date,time,alarm);

                contactList.add(dietModel);

                cursor.moveToNext();
            }



        }
        return contactList;
    }

    // delete contact
    public void deleteContact (int id){
        database =helper.getWritableDatabase() ;
        String whereClause = DataBaseHelper.COL_DIET_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.delete(DataBaseHelper.DIET_TABLE_NAME, whereClause, whereArgs );
        database.close();
    }

    public void updateContact (int id){
        database = helper.getReadableDatabase();
        String whereClause = DataBaseHelper.COL_ID + " = ?";
        String whereArgs[] = {String.valueOf(id)};

        database.update(DataBaseHelper.DIET_TABLE_NAME, null, whereClause, whereArgs );
        database.close();
    }

}


